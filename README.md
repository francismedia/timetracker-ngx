# Timetracker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.
Setup Nodejs first at `https://bitbucket.org/francismedia/starshot-timetracker-api/`

Run `npm i`

Run `ng serve`

Navigate to `http://localhost:4200/`