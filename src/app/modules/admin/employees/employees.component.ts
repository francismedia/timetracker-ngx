import { Component, OnInit, ComponentRef } from '@angular/core';
import { EmployeeService } from 'src/app/core/services/employee.service';
import * as Swal from 'sweetalert2';
import { MzModalService, MzModalComponent } from 'ngx-materialize';
import { EmployeeModalComponent } from '../employee-modal/employee-modal.component';
import { EmployeeModalEditComponent } from '../employee-modal-edit/employee-modal-edit.component';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.sass']
})
export class EmployeesComponent implements OnInit {
  employees: any;
  swal: any = Swal;
  search: string;
  hideInactive: boolean;
  constructor(private employeeService: EmployeeService, private modalService: MzModalService) {
    this.employeeService.employeeUpdated$.subscribe(() => this.getEmployees());
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees() {
    this.employeeService.getEmployees()
      .subscribe((data: any) => {
        if (data) this.employees = data.employees;
        this.employees.forEach((val: any) => {
          if (val.status === 'true') val.status = true;
          val.showTimesheet = false;
        });
      });
  }

  openEmployeeModal() {
    this.modalService.open(EmployeeModalComponent);
  }


}
