import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-employee-status',
  templateUrl: './employee-status.component.html',
  styleUrls: ['./employee-status.component.sass']
})
export class EmployeeStatusComponent implements OnInit {
  @Input() status: boolean;
  constructor() { }

  ngOnInit() {
  }

}
