import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MzBaseModal, MzSelectModule, MzModalService, MzModalComponent } from 'ngx-materialize';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-employee-modal',
  templateUrl: './employee-modal.component.html',
  styleUrls: ['./employee-modal.component.sass']
})
export class EmployeeModalComponent extends MzBaseModal {
  EmployeeForm: FormGroup;
  constructor(private employeeService: EmployeeService, private modalService: MzModalService) {
    super();
    this.EmployeeForm = new FormGroup({
      name: new FormControl(''),
      status: new FormControl(true),
    });
  }

  postEmployee() {
    if (this.EmployeeForm.valid) {
      this.employeeService
        .postEmployee(this.EmployeeForm.value);

      this.EmployeeForm.reset();
      this.modalComponent.closeModal();
    }
  }

}
