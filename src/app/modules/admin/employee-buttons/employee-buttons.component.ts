import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from 'src/app/core/services/employee.service';
import * as Swal from 'sweetalert2';
import { MzModalService } from 'ngx-materialize';
import { EmployeeModalEditComponent } from '../employee-modal-edit/employee-modal-edit.component';
import { Employee } from 'src/app/core/models/employee.model';
@Component({
  selector: 'app-employee-buttons',
  templateUrl: './employee-buttons.component.html',
  styleUrls: ['./employee-buttons.component.sass']
})
export class EmployeeButtonsComponent implements OnInit {
  @Input() employee: Employee;
  toast: any;
  swal: any = Swal;
  constructor(private employeeService: EmployeeService, private modalService: MzModalService) { }

  ngOnInit() {
    this.toast = this.swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
  }

  toggleTimesheet() {
    this.employee.showTimesheet = !this.employee.showTimesheet;
  }

  deleteEmployee(id: number) {
    this.swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result: any) => {
      if (result.value) {
        this.employeeService
          .deleteEmployee(id);

        this.swal.fire(
          'Deleted!',
          'Employee has been deleted.',
          'success'
        )
      }
    });
  }

  openEditEmployeeModal(employee: Employee) {
    this.modalService.open(EmployeeModalEditComponent, { employee });
  }
}
