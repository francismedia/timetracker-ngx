import { Component, OnInit, Input } from '@angular/core';
import { Employee } from 'src/app/core/models/employee.model';
import { FlatpickrOptions } from 'ng2-flatpickr';
import { EmployeeService } from 'src/app/core/services/employee.service';
import * as Swal from 'sweetalert2';

@Component({
  selector: 'app-timesheet-table',
  templateUrl: './timesheet-table.component.html',
  styleUrls: ['./timesheet-table.component.sass']
})
export class TimesheetTableComponent implements OnInit {
  @Input() timesheet: any;
  @Input() employee: Employee;
  swal: any = Swal;
  dateTimeOptions: FlatpickrOptions = {
    enableTime: true,
    dateFormat: "Y-m-d H:i",
  }

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
  }

  editTime(time: any) {
    time.isEditing = !time.isEditing;
  }

  addTime() {
    this.employee.timesheet.push({});
  }

  saveTime(time: any) {
    time.isEditing = !time.isEditing;
    this.employeeService.putEmployee(this.employee, this.employee.id, false);
  }

  deleteTime(index: number) {
    this.swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result: any) => {
      if (result.value) {
        this.employee.timesheet.splice(index, 1);
        this.employeeService.putEmployee(this.employee, this.employee.id, false);
    
        this.swal.fire(
          'Deleted!',
          'Time has been removed.',
          'success'
        )
      }
    });
  }
}
