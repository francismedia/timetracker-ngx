import { Component, OnInit, Input } from '@angular/core';
import { MzBaseModal, MzModalService, MzModalContentDirective } from 'ngx-materialize';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { Employee } from 'src/app/core/models/employee.model';

@Component({
  selector: 'app-employee-modal-edit',
  templateUrl: './employee-modal-edit.component.html',
  styleUrls: ['./employee-modal-edit.component.sass']
})
export class EmployeeModalEditComponent extends MzBaseModal {
  employee: Employee;
  EmployeeForm: FormGroup;
  constructor(private employeeService: EmployeeService, private modalService: MzModalService) {
    super();
  }

  ngOnInit() {
    this.EmployeeForm = new FormGroup({
      name: new FormControl(this.employee.name),
      status: new FormControl(this.employee.status),
    });
  }
  updateEmployee() {
    const data = this.EmployeeForm.value;
    data.timesheet = this.employee.timesheet;

    if (this.EmployeeForm.valid && this.employee && this.employee.id) {
      this.employeeService
        .putEmployee(data, this.employee.id);
      this.modalComponent.closeModal();
    }
  }

}
