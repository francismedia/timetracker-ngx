import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { EmployeesComponent } from './employees/employees.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CoreModule } from 'src/app/core/core.module';
import { RouterModule } from '@angular/router';
import { EmployeeButtonsComponent } from './employee-buttons/employee-buttons.component';
import { TimesheetTableComponent } from './timesheet-table/timesheet-table.component';
import { EmployeeStatusComponent } from './employee-status/employee-status.component';
import { EmployeeModalComponent } from './employee-modal/employee-modal.component';
import { MzModalModule, MzButtonModule, MzSelectModule, MzInputModule, MzChipModule, MzDatepickerModule, MzTimepickerModule, MzRadioButtonModule, MzSwitchModule, MzCheckboxModule } from 'ngx-materialize';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeModalEditComponent } from './employee-modal-edit/employee-modal-edit.component';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';

@NgModule({
  declarations: [
    EmployeesComponent,
    DashboardComponent,
    EmployeeButtonsComponent,
    TimesheetTableComponent,
    EmployeeStatusComponent,
    EmployeeModalComponent,
    EmployeeModalEditComponent,
  ],
  imports: [
    ReactiveFormsModule,
    MzRadioButtonModule,
    AdminRoutingModule,
    MzTimepickerModule,
    MzDatepickerModule,
    Ng2FlatpickrModule,
    MzCheckboxModule,
    MzButtonModule,
    MzSelectModule,
    MzModalModule,
    MzSwitchModule,
    MzInputModule,
    CommonModule,
    RouterModule,
    MzChipModule,
    FormsModule,
    CoreModule
  ],
  entryComponents: [
    EmployeeModalComponent,
    EmployeeModalEditComponent
  ]
})
export class AdminModule { }
