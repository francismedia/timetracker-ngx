import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PublicRoutingModule } from './public-routing.module';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CoreModule } from 'src/app/core/core.module';
import { RouterModule } from '@angular/router';
import { LoginFormComponent } from './login-form/login-form.component';

@NgModule({
  declarations: [LoginComponent, HomeComponent, LoginFormComponent],
  imports: [
    CommonModule,
    CoreModule,
    PublicRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class PublicModule { }
