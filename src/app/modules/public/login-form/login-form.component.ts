import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { FormGroup, FormControl } from '@angular/forms';
import * as Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.sass']
})
export class LoginFormComponent implements OnInit {
  LoginForm: FormGroup;
  Swal: any = Swal;
  constructor(private authService: AuthService, public router: Router) {
    this.LoginForm = new FormGroup({
      username: new FormControl('test'),
      password: new FormControl('test'),
    });
  }

  ngOnInit() {
  }

  postLogin() {
    this.authService
      .login(this.LoginForm.value)
      .subscribe((data: any) => {
        const Toast = this.Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        if (data && data.token) {
          localStorage.setItem('token', data.token);
          Toast.fire({
            type: 'success',
            title: 'Signed in successfully'
          });
          this.router.navigate(['admin'])
        } else {
          Toast.fire({
            type: 'error',
            title: 'Sign in failed'
          });
        }
      })
  }
}
