import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: '',
  loadChildren: () => import('./modules/public/public.module').then(mod=> mod.PublicModule)
},
{
  path: 'admin',
  loadChildren: () => import('./modules/admin/admin.module').then(mod=> mod.AdminModule)
}];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
