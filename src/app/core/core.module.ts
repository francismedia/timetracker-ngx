import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './shared/header/header.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './shared/footer/footer.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptorService } from './interceptors/token-interceptor.service';
import { AuthService } from './services/auth.service';
import { EmployeeService } from './services/employee.service';
import { FilterPipe } from './pipes/filter.pipe';
import { ActiveFilterPipe } from './pipes/active-filter.pipe';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, FilterPipe, ActiveFilterPipe],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    CommonModule,
    HeaderComponent,
    FilterPipe,
    ActiveFilterPipe,
    FooterComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthService,
    EmployeeService
  ]
})

export class CoreModule { }
