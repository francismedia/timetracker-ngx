import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activeFilter'
})
export class ActiveFilterPipe implements PipeTransform {

  transform(items: any, filter?: any): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => item.status);
  }

}
