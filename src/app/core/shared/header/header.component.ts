import { Component, OnInit } from '@angular/core';
import Typed from 'typed.js';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  loggedIn: boolean;
  constructor(private auth: AuthService, public router: Router) { }

  ngOnInit() {
    const typed = new Typed('#logo-container', {
      strings: ["Timetracker", "Starshot Software"],
      typeSpeed: 100,
      loop: true,
      cursorChar: '',
      smartBackspace: false,
    });
    this.isLoggedIn();
  }

  isLoggedIn() {
    this.loggedIn = window.localStorage.getItem('token') ? true : false;
    return this.loggedIn;
  }

  logout() {
    this.auth.logout();
    this.router.navigate([''])
  }
}
