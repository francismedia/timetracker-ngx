export interface Employee {
    id?: number,
    name: string,
    status: boolean,
    timesheet?: [{
        in?: Date,
        out?: Date
    }],
    showTimesheet?: boolean
}