import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  public employeeUpdated$: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private http: HttpClient) { }

  getEmployees() {
    return this.http.get(`${environment.apiUrl}employees`);
  }

  getEmployee(id: number) {
    return this.http.get(`${environment.apiUrl}employees/${id}`);
  }

  postEmployee(data: object) {
    return this.http.post(`${environment.apiUrl}employees`, data)
      .subscribe(() => {
        this.employeeUpdated$.emit(true);
      });
  }

  putEmployee(data: object, id: number, refresh: boolean = true) {
    return this.http.put(`${environment.apiUrl}employees/${id}`, data)
      .subscribe(() => {
        if (refresh)
          this.employeeUpdated$.emit(true);
      });
  }

  deleteEmployee(id: number) {
    return this.http.delete(`${environment.apiUrl}employees/${id}`)
      .subscribe(() => {
        this.employeeUpdated$.emit(true);
      });
  }
}
